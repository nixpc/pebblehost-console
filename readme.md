# PebbleHost Control Panel
This is an **unofficial** control panel for PebbleHost's minecraft servers.

## Screenshots
![Login](./screenshots/login.png)
![Control Panel](./screenshots/console.png)

## Download
[Release v1](https://gitlab.com/nixpc/pebblehost-console/uploads/da08426d9738724a9f6a94780ada1d56/PebbleHost_Control_Panel_v1.zip)