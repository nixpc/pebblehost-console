from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk
import requests
import threading
import time
import sys


running = True
csrf = ""
browser = requests.session()
loggedIn = False
serverid = None
oldLogs = []
oldChats = []

def setCSRF():
    global browser
    global csrf
    req = browser.get("https://mc.pebblehost.com/site/login")
    p = r"value=\"(.*?)\" name=\"YII_CSRF_TOKEN\""
    c = req.text
    csrf = re.search(p, c).group(1)

def popserverlistup(req):
    p = r"<a href=\"/server/([0-9]*)\">(.*?)</a> </div>"
    c = req.text
    matches = re.findall(p, c)
    realmatches = []
    for i in range(len(matches)):
        if i%2==1:
            realmatches.append(matches[i])

    popup = Toplevel(window)
    popup.iconbitmap("./img/favicon.ico")
    list = Listbox(popup)
    for i in realmatches:
        list.insert(END, i[0] + " - " + i[1])
    list.pack()

    def onclick(e):
        global serverid
        serverid = realmatches[list.curselection()[0]][0]
        popup.destroy()

    list.bind("<<ListboxSelect>>", onclick)

    window.wait_window(popup)

def login(username, password):
    global csrf
    global browser
    global loggedIn
    global serverid

    if csrf == "":
        setCSRF()

    data = {
        "YII_CSRF_TOKEN": csrf,
        "LoginForm[name]": username,
        "LoginForm[password]": password,
        "LoginForm[rememberMe]": 0,
        "LoginForm[ignoreIp]": 0,
        "LoginForm[ignoreIp]": 1,
    }

    req = browser.post("https://mc.pebblehost.com/site/login", data=data)
    if req.url != "https://mc.pebblehost.com/site/login":
        loggedIn = True
        p = r"server/(.*)"
        c = req.url
        serverid = re.search(p, c).group(1)
        if serverid == "index?my=1":
            popserverlistup(req)
        window.destroy()


def loginkeypress(key):
    if key.keycode == 13: # Return
        login(username.get(), password.get())


def commandEnter(key):
    if key.keycode == 13: # Return
        global browser
        global serverid
        global csrf
        req = browser.post("https://mc.pebblehost.com/server/log/" + serverid, data={"ajax": "command", "YII_CSRF_TOKEN": csrf, "command": commandString.get()})
        commandString.set("")
        refreshConsole()
        console.yview(END)


def refreshConsole():
    global browser
    global serverid
    global csrf
    global oldLogs
    req = browser.post("https://mc.pebblehost.com/server/log/" + serverid, data={"ajax":"refresh", "type":"all", "YII_CSRF_TOKEN":csrf})
    logs = req.json()["log"].splitlines()

    if len(oldLogs) == 0:
        for i in logs:
            console.insert(0, i)
        oldLogs = logs
        return

    toAdd = []

    for i in range(0, len(logs)):
        if logs[i] == oldLogs[0]:
            break

        toAdd.append(logs[i])

    for i in range(len(toAdd) - 1, -1, -1):
        console.insert(END, toAdd[i])

    oldLogs = logs

def refreshChat():
    global browser
    global serverid
    global csrf
    global oldChats
    req = browser.post("https://mc.pebblehost.com/server/chat/" + serverid, data={"ajax":"refresh", "type":"all", "YII_CSRF_TOKEN":csrf})
    logs = req.json()["chat"].splitlines()

    if len(oldChats) == 0:
        for i in logs:
            chat.insert(END, i)
        oldChats = logs
        return

    toAdd = []

    for i in range(len(logs)-1, -1, -1):
        if logs[i] == oldChats[len(oldChats)-1]:
            break

        toAdd.append(logs[i])

    for i in range(len(toAdd) - 1, -1, -1):
        chat.insert(END, toAdd[i])

    oldChats = logs


def startRefreshing():
    def loop():
        while running:
            try:
                if scrollbar.get()[1] == 1:
                    refreshConsole()
                    console.yview(END)
                else:
                    refreshConsole()
                time.sleep(0.5)
            except:
                pass

    threading.Thread(target=loop).start()

def startRefreshing2():
    def loop():
        while running:
            try:
                if scrollbar2.get()[1] == 1:
                    refreshChat()
                    chat.yview(END)
                else:
                    refreshChat()
                time.sleep(0.5)
            except:
                pass

    threading.Thread(target=loop).start()


def restartServer():
    global browser
    global serverid
    global csrf
    req = browser.post("https://mc.pebblehost.com/server/" + serverid, data={"ajax": "restart", "YII_CSRF_TOKEN": csrf})

def startServer():
    global browser
    global serverid
    global csrf
    req = browser.post("https://mc.pebblehost.com/server/" + serverid, data={"ajax": "start", "YII_CSRF_TOKEN": csrf})

def stopServer():
    global browser
    global serverid
    global csrf
    req = browser.post("https://mc.pebblehost.com/server/" + serverid, data={"ajax": "stop", "YII_CSRF_TOKEN": csrf})


### Create the login window
window = Tk()
window.geometry("400x300")
window.title("Multicraft Login (unofficial)")
window.iconbitmap("./img/favicon.ico")

logo = ttk.Label(window)
logoImage = ImageTk.PhotoImage(Image.open('./img/logo.png'))
logo["image"] = logoImage
logo.pack()

username = StringVar()
usernameLabel = Label(window, text="Email")
usernameLabel.pack()
usernameEntry = Entry(window, textvariable=username)
usernameEntry.pack()
usernameEntry.focus_force()
usernameEntry.bind("<KeyPress>", loginkeypress)

password = StringVar()
passwordLabel = Label(window, text="Password")
passwordLabel.pack(pady=(8, 0))
passwordEntry = Entry(window, textvariable=password, show="•")
passwordEntry.pack()
passwordEntry.bind("<KeyPress>", loginkeypress)

window.mainloop()


### Create the control panel
if not loggedIn:
    sys.exit()

if not serverid:
    sys.exit()

window = Tk()
window.geometry("800x600")
window.title("PebbleHost Console (unofficial)")
window.iconbitmap("./img/favicon.ico")
ttk.Sizegrip(window).grid(column=3, row=1, sticky=(S,E))

# Create the menu bar
window.option_add("*tearOff", FALSE)
menubar = Menu(window)
window["menu"] = menubar

menubar.add_command(label="Start", command=startServer)
menubar.add_command(label="Stop", command=stopServer)
menubar.add_command(label="Restart", command=restartServer)

# Create the console
console = Listbox(window)
console.grid(column=0, row=0, sticky=(N, W, E, S))
scrollbar = ttk.Scrollbar(window, orient=VERTICAL, command=console.yview)
scrollbar.grid(column=1, row=0, sticky=(N, S))
console["yscrollcommand"] = scrollbar.set
window.rowconfigure(0, weight=1)
window.columnconfigure(0, weight=1)
startRefreshing()

# Create the chat
chat = Listbox(window)
chat.grid(column=2, row=0, sticky=(N, W, E, S))
scrollbar2 = ttk.Scrollbar(window, orient=VERTICAL, command=chat.yview)
scrollbar2.grid(column=3, row=0, sticky=(N, S))
chat["yscrollcommand"] = scrollbar2.set
window.rowconfigure(0, weight=1)
window.columnconfigure(2, weight=1)
startRefreshing2()

# Create the command bar
commandString = StringVar()
commandBar = Entry(window, textvariable=commandString)
commandBar.grid(column=0, columnspan=3, row=1, sticky=(N, W, E, S))
commandBar.focus_force()
window.rowconfigure(1, weight=0)
commandBar.bind("<KeyPress>", commandEnter)

# Loop
window.mainloop()
running = False
sys.exit()