pyinstaller --onefile --noconsole --icon=img/mainlogo.ico main.py
cd dist
mkdir img
cd ..
copy "img\favicon.ico" "dist\img\favicon.ico"
copy "img\logo.png" "dist\img\logo.png"